import * as Sequelize from "sequelize";
import { SequelizeAttributes } from "typings/SequelizeAttributes";

export interface AuthorizedUserAttributes {
	id?: number,
	slackID: string,
	createdAt?: Date,
	updatedAt?: Date
}

export interface AuthorizedUserInstance extends Sequelize.Instance<AuthorizedUserAttributes>, AuthorizedUserAttributes {
}

export const AuthorizedUserFactory = (sequelize: Sequelize.Sequelize, DataTypes: Sequelize.DataTypes): Sequelize.Model<AuthorizedUserInstance, AuthorizedUserAttributes> => {
	const attributes: SequelizeAttributes<AuthorizedUserAttributes> = {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		slackID: {
			type: DataTypes.STRING,
			unique: true
		}
	};

	const AuthorizedUser = sequelize.define<AuthorizedUserInstance, AuthorizedUserAttributes>("AuthorizedUser", attributes);

	return AuthorizedUser;
}
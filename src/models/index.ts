import * as Sequelize from "sequelize";
import { DbInterface } from "typings/DbInterface";
import { AuthorizedUserFactory } from "models/AuthorizedUser";

export const initDb = (sequelizeConfig?: Sequelize.Options): DbInterface => {
	const sequelize = new Sequelize(sequelizeConfig);

	const db: DbInterface = {
		sequelize: sequelize, 
		Sequelize: Sequelize,
		AuthorizedUser: AuthorizedUserFactory(sequelize, Sequelize)
	}

	Object.keys(db).forEach(modelName => {
		if (db[modelName].associate) {
			db[modelName].associate(db)
		}
	});

	return db;
}
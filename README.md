# Slack REPL

A simple module that allows authorized users to execute commands from within Slack and see the output. Primarily intended for light development and testing when away from a dev computer with a proper REPL on it.
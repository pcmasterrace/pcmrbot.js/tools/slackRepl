module.exports = {
	apps : [{
		name      : 'tool/slackRepl',
		script    : 'build/app.js',
		env: {
			// Either inject the values via environment variables or define them here
			TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
			SLACKREPL_TIMEOUT: process.env.SLACKREPL_TIMEOUT || 1*60*1000,
			SLACKREPL_DB_DIALECT: process.env.SLACKREPL_DB_DIALECT || undefined,
			SLACKREPL_DB_HOST: process.env.SLACKREPL_DB_HOST || undefined,
			SLACKREPL_DB_NAME: process.env.SLACKREPL_DB_NAME || undefined,
			SLACKREPL_DB_USERNAME: process.env.SLACKREPL_DB_USERNAME || undefined,
			SLACKREPL_DB_PASSWORD: process.env.SLACKREPL_DB_PASSWORD || undefined,
			SLACKREPL_DB_PORT: process.env.SLACKREPL_DB_PORT || undefined,
			SLACKREPL_DB_PATH: process.env.SLACKREPL_DB_PATH || undefined,
			SLACKREPL_DB_DROP_TABLES_ON_START: process.env.SLACKREPL_DB_DROP_TABLES_ON_START || false
		}
	}]
};

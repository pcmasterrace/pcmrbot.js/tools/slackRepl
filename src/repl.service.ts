import { Service, Context } from "moleculer";
import yn from "yn";

import { initDb } from "models";
import { DbInterface } from "typings/DbInterface";

/**
 * This module enables REPL-like functionality within Slack. 
 * 
 * @module "slackRepl"
 * @version 1
 */

class SlackReplService extends Service {
    protected db: DbInterface;

    constructor(broker) {
        super(broker);

        this.parseServiceSchema({
            name: "slackRepl",
            version: 1,
            dependencies: [
                {name: "slack.web", version: 1}, 
                {name: "slack.commands", version: 1}
            ],
            actions: {
                addAuthorizedUser: {
                    name: "addAuthorizedUser",
                    params: {
                        slackID: "string"
                    },
                    handler: this.addAuthorizedUser
                }
            },
            events: {
                "slack.commands.callaction": this.callAction,
                "slack.commands.listactions": this.listActions
            },
            started: this.serviceStarted
        })
    }

    /**
     * Calls an action on the microservice network. This command cannot be invoked directly, but it is documented here for completeness.
     * 
     * @function
     * @private
     * @param {object} command - The command payload as returned by Slack.
     */
    async callAction(command) {
        // Check if user is authorized
        if (await !this.isUserAuthorized(command.user_id)) {
            await this.broker.call("v1.slack.web.postEphemeral", {
                channel: command.channel_id,
                user: command.user_id,
                message: "*Warning*: You are not authorized to use this command"
            });
            return;
        }

        let opts = this.parseOptions(command.text, true);
        
        let action = opts.shift();
        let params = {};
        while (opts.length >= 2) {
            try{
                params[opts[0]] = opts[1];
                opts.splice(0, 2);
            } catch (err) {
                break;
            }
        }

        let res = await this.broker.call(action, params);

        await this.broker.call("v1.slack.web.postMessage", {
            channel: command.channel_id,
            message: JSON.stringify(res, null, "\t")
        });
    }

    /**
     * Lists all registered actions on the microservice network. This command cannot be invoked directly, but it is documented here for completeness.
     * 
     * @function
     * @private
     * @param {object} command - The command payload as returned by Slack.
     */
    async listActions(command) {
        // Check if user is authorized
        if (await !this.isUserAuthorized(command.user_id)) {
            await this.broker.call("v1.slack.web.postEphemeral", {
                channel: command.channel_id,
                user: command.user_id,
                message: "*Warning*: You are not authorized to use this command"
            });
            return;
        }

        let actions = await this.broker.call("$node.actions");

        let md = "";
        for (let action of actions) {
            if (action.name.includes(command.text.trim())) {
                let params = action.action.params || {};
                let paramList = Object.keys(params).map(key => {
                    let type = typeof params[key] === "object" ? params[key].type : params[key];
                    return `>  * ${params[key].optional?"[":""}${key}${params[key].optional?"]":""} (${type})`
                });
                md += `*${action.name}*\n>*Node count:* ${action.count}\n>*Cached?:* ${action.action.cache}\n>*Parameters:*\n${paramList.join("\n")}\n\n`;
            }
        }

        await this.broker.call("v1.slack.web.postMessage", {
            channel: command.channel_id,
            message: md
        });
    }

    /**
     * Checks whether a given user ID is authorized to execute commands.
     * 
     * @function
     * @private
     * @param {string} slackID - The Slack user ID to check
     * @returns {boolean} Whether the user is authorized
     */
    async isUserAuthorized(slackID: string): Promise<boolean> {
        return await this.db.AuthorizedUser.count({where: {slackID: slackID}}) === 1
    }

    /**
     * Allows a new user to use the REPL. 
     * 
     * @function
     * @static
     * @package
     * @name addAuthorizedUser
     * @param {string} slackID - The Slack user ID you want to add.
     */
    async addAuthorizedUser(ctx: Context) {
        await this.db.AuthorizedUser.create({
            slackID: ctx.params.slackID
        });
    }

    // Command parser taken from here: 
    // http://krasimirtsonev.com/blog/article/Simple-command-line-parser-in-JavaScript
    parseOptions (optStr: string, lookForQuotes: boolean) {
        let args = [];
        let readingPart = false;
        let part = '';
        for(let i = 0; i < optStr.length; i++) {
            if(optStr.charAt(i) === ' ' && !readingPart) {
                args.push(part);
                part = '';
            } else {
                if(optStr.charAt(i) === '\"' && lookForQuotes) {
                    readingPart = !readingPart;
                } else {
                    part += optStr.charAt(i);
                }
            }
        }
        args.push(part);
        return args;
    }

    async serviceStarted() {
        this.db = initDb({
			dialect: process.env.SLACKREPL_DB_DIALECT || undefined,
			database: process.env.SLACKREPL_DB_NAME || undefined,
			host: process.env.SLACKREPL_DB_HOST || undefined,
			username: process.env.SLACKREPL_DB_USERNAME || undefined,
			password: process.env.SLACKREPL_DB_PASSWORD || undefined,
			port: Number(process.env.SLACKREPL_DB_PORT) || undefined,
			storage: process.env.SLACKREPL_DB_PATH || undefined,
			operatorsAliases: false,
			logging: false
        });

		await this.db.sequelize.sync({force: yn(process.env.SLACK_REPL_DB_DROP_TABLES_ON_START, {default: false})});
    }
}

module.exports = SlackReplService;
import * as Sequelize from "sequelize";
import { AuthorizedUserInstance, AuthorizedUserAttributes } from "models/AuthorizedUser";

export interface DbInterface {
	sequelize: Sequelize.Sequelize;
	Sequelize: Sequelize.SequelizeStatic;
	AuthorizedUser: Sequelize.Model<AuthorizedUserInstance, AuthorizedUserAttributes>;
}